MH EQU 60H
ML EQU 61H
NH EQU 62H
NL EQU 63H

MLNLH EQU 70H
MLNLL EQU 71H
MHNLH EQU 72H
MHNLL EQU 73H
MLNHH EQU 74H
MLNHL EQU 75H
MHNHH EQU 76H
MHNHL EQU 77H

MOV R0,#092H
MOV R1,#0A4H
MOV R2,#02FH
MOV R3,#0DDH

MOV A,R0
MOV B,R1
MUL AB

MOV MH,B
MOV ML,A

MOV A,R2
MOV B,R3
MUL AB

MOV NH,B
MOV NL,A

MOV A,ML
MOV B,NL
MUL AB
MOV MLNLH,B
MOV MLNLL,A

MOV A,MH
MOV B,NL
MUL AB
MOV MHNLH,B
MOV MHNLL,A

MOV A,ML
MOV B,NH
MUL AB
MOV MLNHH,B
MOV MLNHL,A

MOV A,MH
MOV B,NH
MUL AB
MOV MHNHH,B
MOV MHNHL,A

MOV P0,MLNLL

;MOV A,MHNLL
;MOV A,MLNHL
MOV A,MLNLH
ADD A,MHNLL
ADDC A,MLNHL

MOV P1,A

;MOV A,MHNHH
;MOV A,MHNHL
MOV A,MHNLH
ADDC A,MLNHH
ADDC A,MHNHL

MOV P2,A

MOV P3,MHNHH
  SJMP $